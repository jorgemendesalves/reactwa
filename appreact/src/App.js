import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

const H1 = ({classNameAcolocar, prefixoDeTexto, children}) => {
  return <div className = { classNameAcolocar}> {prefixoDeTexto} o nosso h1 {children}</div>
}
const ul = ({className}) =>{
  return

}

/*
function H1 (props){
  return(
    <div className={props.classNameAcolocar}>
    {props.prefixoDeTexto} o nosso h1 {props.children}
    </div>

  )
}
*/

const Todo = ({titulo, data, estado}) => {
  return(
    <li>
      <span>{titulo}</span>
      <span>{data.toLocaleString()}</span>
      <span>{estado ? "Sim" : "Não"}</span>
      </li>
  )
}

const TodoList = ()=>{
  this.setState({
    lista: [],})
  return(
<ul>
 <Todo titulo="ir à pesca"
       data = {new Date (2018, 4, 3)}
       estado = {false}/>
 <Todo titulo="ir à pesca"
       data = {new Date (2018, 4, 3)}
       estado = {false}/>
 <Todo titulo="ir à pesca"
       data = {new Date (2018, 4, 3)}
       estado = {false}/>
</ul>

  )
}







class App extends Component {
  constructor(props){
    super(props); // sempre invocar este/ constructor da class Component
    this.state= {
      newDate: new Date(),
      
      ticking: true,
      frase: "ainda n está aleatoria",
      frases: [],
      newName :"Roman Todd5",
      frase_adicionar:"",
      estado_frase:"",
      updade_state_message:"",
      frase_alterar:""
    }
    this.toggleTick = this.toggleTick.bind(this);
    this.handleNewPhaseclick = this.handleNewPhaseclick.bind(this);
    this.handleNewAddNameClick = this.handleNewAddNameClick.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmitForm = this.handleSubmitForm.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
  }


  //por ordem 
  componentWillMount(){
   // console.log('componentWillMount');
    this.setupTick(this.state.ticking)
    this.setRadomPhase()
    // bind é um metodo para definir o scope de execução da função 
    this.getLocalFrases()
  }
  componentDidMount(){
    //console.log('componentDidMount');
    this.frase_adicionar.focus()


  }
  componentWillReceiveProps(){
   // console.log('componentWillReceiveProps');
  }
  shouldComponentUpdate(){
   // console.log('shouldComponentUpdate');
    return true; //this.state.newDate.getSeconds()% 2 === 0 ? true : false ;
  }
  componentWillUpdate(){
    //console.log('componentWillUpdate');
  }
  componentDidUpdate(){
    //console.log('componentDidUpdate');
  }
  componentWillUnmount(){
    //console.log('componentWillUnmount');
  }
  setRadomPhase() {
    const frasesAUtilizar= this.state.frases
     
    let fraseIndex = Math.round(Math.random()* (frasesAUtilizar.length -1));
    this.setState({frase: frasesAUtilizar[fraseIndex]});
  }
  tick(){
    this.setState({newDate: new Date()})
  }
handleNewPhaseclick(){
this.setRadomPhase()

}

handleNewAddNameClick(){
  this.addNewName()
}
addNewName(){

const frases = this.state.frases
  frases.push(this.state.newName)
  

  

  

}





  setupTick(doTick) {
    if(doTick){
      this.interval = setInterval(this.tick.bind(this), 1000)
      this.tick()
    } else {
      clearInterval(this.interval);
    }
  }

  toggleTick(){
    this.setState(prevState => {
      let nextTickState = !this.state.ticking;
      this.setupTick(nextTickState);
      console.log(nextTickState)
      return {
      ticking: nextTickState  
        }
    });
    
  }
  handleInputChange (event) {
   
    this.setState({
      [event.target.name]:event.target.value
    })

  }
  handleSubmitForm(event){
    event.preventDefault();
if(this.state.frase_adicionar !==""){
  this.state.frases.push(this.state.frase_adicionar)
this.setState({
  frases: this.state.frases,
  frase_adicionar:""
})
this.setLocalFrases(this.state.frases)
this.frase_adicionar.focus()
  }
  else{
    alert("TENS QUE ESCREVER!!!!!")
  }
  this.setLocalFrases(this.state.frases)
}
handleRemove(fraseIndex, e){
  this.state.frases.splice(fraseIndex ,1)
  this.setState({frases: this.state.frases})
  this.frase_adicionar.focus()
  
  this.setLocalFrases(this.state.frases)

}
handleEdit(fraseIndex){
    const frases = this.state.frases
    frases[fraseIndex] = this.state.frase_alterar
    this.setState({
        frases,
        frase_alterar:""
    })
   
    this.setLocalFrases(this.state.frases)


}
getLocalFrases(){
  let frases = localStorage.getItem("frases")
  console.log("frases", frases);
  if(frases === null){
    frases =[];
  }
  else{
    frases = JSON.parse(frases)
  }
  this.setState({frases})
}
  setLocalFrases(frases){
    this.setState({updade_state_message:"a gravar dados localmente..."})
    localStorage.setItem("frases", JSON.stringify(frases))
    setTimeout(()=>{
      this.setState({updade_state_message:""})
    },2000)
    
  
  
  

  }
  render() { 
    console.log('RENDER')
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <H1 className="App-title" prefixoDeTexto="Prefixo do texto do header"> Welcome to React </H1>
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          Abaixo encontra-se uma lista :V
        </p>
        <div className="Todo">
            <TodoList/>
        </div>
        <div>
          { !!this.state.newDate ? this.state.newDate.toLocaleString() : "" }
        </div>
        <div>
          { this.state.ticking ? "Ticking" : "Not Ticking"}
        </div>
        <div>
          <button onClick={this.toggleTick}>
           { this.state.ticking ? "Parar Relógio" : "Iniciar relógio"}</button>
        </div>
        <div><p>Frase aleatorio: {this.state.frase}</p></div>
        <button onClick={this.handleNewPhaseclick}>tryme</button>
        <div>
          <ul>
            {this.state.frases.map((item, index) => { 
            return <li key={"frase" + index}>{item}
           <button onClick={this.handleRemove.bind(this, index)}>Remove</button>
          
           <input type="text" name="frase_alterar" value={this.state.frase_alterar}  onChange={this.handleInputChange}/>
           <button onClick={this.handleEdit.bind(this, index)}>Edit</button> 
            </li>
            })}
          </ul>
        </div>
        <button onClick={this.handleNewAddNameClick}>Addname!</button>
        <div>
          {this.state.updade_state_message}
          <form onSubmit={this.handleSubmitForm}>
          <input type="text" name="frase_adicionar" value={this.state.frase_adicionar} onChange={this.handleInputChange} ref={el => this.frase_adicionar = el}/>
          <select name="estado_frase" value={this.state.estado_frase}onChange={this.handleInputChange}>
          <option value="feito">Feito</option>
          <option value="por_fazer">Por Fazer</option>
          </select>
          <input type="submit" name="frase_submit" />
          
          </form>
          <p>{this.state.frase_adicionar}</p>
        </div>
      </div>

    );
  }
}

export default App;
