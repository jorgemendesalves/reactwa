import React from 'react';
import { StyleSheet, Text, View, Button, TextInput  } from 'react-native';
import List from './Todo/List';

export default class App extends React.Component {
  


  render() {
    return (
      <View style={styles.container}>
       <List/>
        <TextInput type="text" name="text" />
        
        <Button title='Add'></Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
   
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
