import React, { Component } from 'react';
import {SectionList, StyleSheet, Text, View } from 'react-native';
import Item from './Item'


class List  extends Component {
  


  render() {
      return (
        <View style={styles.container}>
       <SectionList
  renderItem={({item, index, section}) => <Item data={item} index={index}>{item}</Item>}
  renderSectionHeader={({section: {title}}) => (
    <Text style={{fontWeight: 'bold'}}>{title}</Text>
  )}
  sections={[
    {title: 'Title1', data: [data[1], 'item2']},
    {title: 'Title2', data: ['item3', 'item4']},
    {title: 'Title3', data: ['item5', 'item6']},
  ]}
  keyExtractor={(item, index) => item + index}
/>
    </View>

      );
    }
  }
  const styles = StyleSheet.create({
    container: {
      flex: 1,
     
     
      
      alignItems: 'center',
      justifyContent: 'center',
    },
  });


export default List;