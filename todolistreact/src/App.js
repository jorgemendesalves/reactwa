import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
/*
const Todo = ({item, estado, index}) => {
  
    return(
     <li key={"frase" + index}>
     <span>{item}</span>
    
     <span>{estado ? "Sim" : "Não"}</span>
     </li>
     )
  
    }
*/
const TodoList = ({lista, item_alterar, handleRemove,  handleSubmitEdit, handleUpdate})=>{
  return(
    <ul>
    {
      lista.map((item, index) => {
      return <li key={"frase" + index}>
        <span>{item.nome}</span>
      
        <span>{item.estado ? " Sim " : " Não "}</span>
       
        <button onClick={ handleSubmitEdit.bind(index, this)}>Edit</button>
           <input type="text" name="item_alterar" value={item_alterar} onUpdate={handleUpdate} />
           <button onClick={handleRemove.bind(index, this)}>Remove</button>
      </li>
      })
    }
    </ul>
  )}
 
  
  
  
  
  
  
  
  class App extends Component {
    constructor(props){
      super(props); // sempre invocar este/ constructor da class Component
      this.state= {
        lista: [],
        item_adicionar:"",
        item_estado:"",
        
      } 
      this.handleInputChange = this.handleInputChange.bind(this);
      this.handleSubmitForm = this.handleSubmitForm.bind(this);
      this.handleUpdate = this.handleUpdate.bind(this);
      this.handleRemove = this.handleRemove.bind(this)
      
    }
    
    
    handleSubmitForm(event){
      event.preventDefault();
      if(this.state.item_adicionar !==""){
        this.state.lista.push({
          nome:this.state.item_adicionar,
          estado:this.state.item_estado,


        })

        
        
        
        this.setState({
          
          lista:this.state.lista, 
          
          item_adicionar:"",
          item_estado:"",
        })
       
        
      }
      else{
        alert("TENS QUE ESCREVER!!!!!")
      }
     
    }
    handleSubmitEdit(event){
     
      if(this.state.item_alterar !==""){
        this.state.lista.push({
          item_alterar:this.state.item_alterar.value


        })

        
        
        
        this.setState({
          
          lista:this.state.lista, 
          
          item_adicionar:"",
          item_estado:"",
        })
       
        
      }
      else{
        alert("TENS QUE ESCREVER!!!!!")
      }
     
    }
    
    getLocalFrases(){
      let lista = localStorage.getItem("frases")
      console.log("frases", lista);
      if(lista === null){
        lista =[];
      }
      else{
        lista = JSON.parse(lista)
      }
      this.setState({lista})
    }
    
    
    
    handleInputChange (event) {
      
      this.setState({
        [event.target.name]:event.target.value
      })
      
    }
    
     handleRemove(fraseIndex, e){
      this.state.lista.splice(fraseIndex ,1)
      this.setState({lista: this.state.lista})
      
      
      
    
    }

    handleUpdate(fraseIndex, data){
      const lista = this.state.lista
      lista[fraseIndex] = data
      this.setState({
        lista:this.state.lista
      })
     
      
  
  
  }
    
    render() {
      return (
        <div className="App">
        
        <header className="App-header">
        <h1 className="App-logo">List</h1>
        <h2>Buy groceries<span>{this.state.lista.length}</span></h2>
        </header>

        <TodoList
         lista={this.state.lista}  
         item_alterar= {this.state.item_alterar} 
         handleRemove={this.handleRemove} 
         onUpdate={this.handleUpdate}
         handleSubmitEdit={this.handleSubmitEdit} />
        
        <form onSubmit={this.handleSubmitForm}>
        <input type="text" name="item_adicionar" value={this.state.item_adicionar} onChange={this.handleInputChange}/>
        <select name="item_estado" value={this.state.item_estado} onChange={this.handleInputChange}>
        <option value={this.state.item_feito}>Feito</option>
        <option value={this.state.item_por_fazer}>Por Fazer</option>
        </select>
        <input type="submit" name="frase_submit" />
        
        </form>
        <p>{this.state.item_adicionar}</p>
        
        
        
        </div>
      );
    }
  }
  
  export default App;
